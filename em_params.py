# -*- coding: utf-8 -*-

from sklearn.mixture import GaussianMixture
from sklearn import metrics
import matplotlib.pyplot as plt
import pandas as pd


def loaddata(filename):
    #Cargamos los datos del excel
    data_xls = pd.read_excel(filename, index=False)
    #Asignamos los nombres de las columnas para coincidir con los del excel
    data_xls.columns=data_xls.iloc[[5]].values.tolist()[0]
    #Eliminamos los primeros campos del excel, que no tienen datos
    data_xls = data_xls.drop([0,1,2,3,4,5])
    #Reseteamos el índice del dataframe
    data_xls = data_xls.reset_index(drop=True)
    return data_xls

#Carga de datos
original_data = loaddata("Renta_local_2007.xlsx")
#Eliminamos las columnas que no consideramos útiles para el cálculo de clusters
discarded_columns=[
        "Nombre municipio",
        "Código INE municipio",
        "Código INE municipio (5 dígitos)",
        "Nombre Comunidad Autónoma",
        "Nombre Provincia",
        #"Índice de Gini", 
        "Índice de Atkinson 0,5",
        "Código INE Comunidad Autónoma ",
        "Código INE Provincia ",
        "Población declarante (IRPF)",
        "Número de observaciones muestrales",
        "Renta imponible agregada (IRPF)", #con este dato EM no funciona
        "Renta imponible media (por declarante)",
        "Top 1%",
        "Top 0,1%",
        "Top 0,5%"]
work_data = original_data.drop(columns = discarded_columns)

#array que relaciona cada fila con un cluster
labels = [0 for x in range(len(work_data))]

#preprocesando datos (normalización)
import sklearn.preprocessing as pre
min_max_scaler = pre.MinMaxScaler()
work_data = min_max_scaler.fit_transform(work_data)

#Vamos a representar graficamente los valores de silhouettes 5 veces
#para identificar falsos positivos que puedan ocurrir. 
for j in range (0,5):
    silhouettes = []
    for i in range(2, 15):
        em = GaussianMixture(n_components=i,covariance_type='full', init_params='kmeans')
        em.fit(work_data) #normalización
        labels =  em.predict(work_data)
        silhouettes.append(metrics.silhouette_score(work_data, labels))
    # Plot Silhouette
    plt.plot(range(2,15), silhouettes, marker='o')
    plt.xlabel('Number of clusters')
    plt.ylabel('Silhouette')
    plt.show()
