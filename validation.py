# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import pandas as pd

def loaddata(filename):
    #Cargamos los datos del excel
    data_xls = pd.read_excel(filename, index=False)
    #Asignamos los nombres de las columnas para coincidir con los del excel
    data_xls.columns=data_xls.iloc[[5]].values.tolist()[0]
    #Eliminamos los primeros campos del excel, que no tienen datos
    data_xls = data_xls.drop([0,1,2,3,4,5])
    #Reseteamos el índice del dataframe
    data_xls = data_xls.reset_index(drop=True)
    return data_xls

def plotdata(data,labels,name): #def function plotdata
#colors = ['black']
    fig, ax = plt.subplots()
    data_x = [data.at[x,"Renta imponible media (por habitante)"] for x in range(len(data))]
    data_y =[data.at[x,"Población (INE)"] for x in range(len(data))]
    plt.yscale('log')
    plt.scatter(data_x, data_y, c=labels)
    ax.grid(True)
    fig.tight_layout()
    plt.title(name)
    plt.show()

#load and plot data
#Carga de datos
original_data = loaddata("Renta_local_2007.xlsx")
#Eliminamos las columnas que no consideramos útiles para el cálculo de clusters
discarded_columns=[
        "Nombre municipio",
        "Código INE municipio",
        "Código INE municipio (5 dígitos)",
        "Nombre Comunidad Autónoma",
        "Nombre Provincia",
        #"Índice de Gini", 
        "Índice de Atkinson 0,5", 
        "Código INE Comunidad Autónoma ",
        "Código INE Provincia ",
        "Población declarante (IRPF)",
        "Número de observaciones muestrales",
        "Renta imponible agregada (IRPF)", #con este dato EM no funciona
        "Renta imponible media (por declarante)",
        "Top 1%",
        "Top 0,1%",
        "Top 0,5%"]
work_data = original_data.drop(columns = discarded_columns)
#array que relaciona cada fila con un cluster
labels = [0 for x in range(len(work_data))]

#preprocesando datos (normalización)
import sklearn.preprocessing as pre
min_max_scaler = pre.MinMaxScaler()
#datos normalizados y listos para introducir en el algoritmo
em_data = min_max_scaler.fit_transform(work_data)

# expectation maximization
from sklearn.mixture import GaussianMixture
em = GaussianMixture(n_components=5, covariance_type='full', init_params='kmeans')
em.fit(em_data)
labels = em.predict(em_data)

#mostramos renta frente a población
plotdata(original_data, labels ,'Expectation Maximization')

#agregamos columna a los datos originales para saber a qué cluster pertenece cada fila
original_data['cluster'] = labels

#matriz para ver las medias de cada cluster
statistics = pd.DataFrame(columns=['n_elem', 'pob_avg', 'rent_avg', 'gini_avg', 'q1', 'q2', 'q3', 'q4', 'q5'])
for i in range(0,5):
    aux = original_data.loc[original_data['cluster'] == i]
    statistics.loc[i] = aux["Población (INE)"].count(), aux["Población (INE)"].mean(), aux["Renta imponible media (por habitante)"].mean(), aux["Índice de Gini"].mean(), aux["Quintil 1"].mean(), aux["Quintil 2"].mean(), aux["Quintil 3"].mean(), aux["Quintil 4"].mean(), aux["Quintil 5"].mean()               

#creamos matriz para ver como están repartidos los clusters en las comunidades autónomas
community_names = original_data["Nombre Comunidad Autónoma"].unique().tolist()
communities_clustered = pd.DataFrame(columns = ['comunidad', 0, 1, 2, 3, 4])

#rellenamos la matriz
for i in range(len(community_names)):
    aux = original_data.loc[original_data["Nombre Comunidad Autónoma"] == community_names[i]]
    communities_clustered.loc[i] = community_names[i],0,0,0,0,0
    for j in range(0,5):
        aux2 = aux.loc[aux["cluster"] == j].count()["cluster"]
        communities_clustered.at[i,j] = aux2